package com.car.price.search;

import com.car.price.search.driver.DriverManager;
import com.car.price.search.util.CommonUtils;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;

public class Hooks {
    private DriverManager driverManager = new DriverManager();
    public static String testDataPropertyFile = "car_details.properties";

    @Before
    public void setUp() {
        CommonUtils.loadTestDataProp(testDataPropertyFile);
        driverManager.openBrowser();
        driverManager.navigateTo(CommonUtils.BASE_APP_URL);
        driverManager.maxBrowser();
        driverManager.applyImplicit();
    }

    @After
    public void tearDown(Scenario scenario) {
        if (scenario.isFailed()) {
            driverManager.takeSceenShot(scenario);
        }
        driverManager.closeBrowser();
    }
}
