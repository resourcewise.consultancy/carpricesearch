package com.car.price.search.util;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import java.util.Random;

public class CommonUtils {
    private static Properties prop = new Properties();
    private static FileInputStream fis;
    public static String BASE_APP_URL;
    public static String BROWSER;
    public static String DRIVER_TIMEOUT;

    public static void loadTestDataProp(String testDataPropertyFile) {
        try {
            fis = new FileInputStream(System.getProperty("user.dir") + "/src/test/resources/properties/" + testDataPropertyFile);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        try {
            prop.load(fis);
        } catch (IOException e) {
            e.printStackTrace();
        }

        BASE_APP_URL = prop.getProperty("base.app.url");
        BROWSER = prop.getProperty("browser");
        DRIVER_TIMEOUT = prop.getProperty("driver.timeout");
    }
}
